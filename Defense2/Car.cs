﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Defense2
{
    class Car
    {
        string model;
        public string _model
        {
            get { return model; }
            set { model = value;}
        }

        int size;
        public int _size
        {
            get { return size; }
            set { size = value; }

        }

        double consumption;
        public double _consumption
        {
            get { return consumption; }
            set { consumption = value; }
        }

        public Car(string model, int size, double consumption)
        {
            this.model = model;
            this.size = size;
            this.consumption = consumption;
        }
    }
}
