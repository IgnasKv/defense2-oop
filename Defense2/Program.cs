﻿using System;
using System.IO;


//algorithms should return objects not indexes
namespace Defense2
{
    class Program
    {
        const int max = 100;

        static void Main(string[] args)
        {
            //Defining data files
            string data = "Data.txt";
            string dataS1 = "Student1.txt";
            string dataS2 = "Student2.txt";

            //creating variable to store student's name
            string name;
            //Creating and filling cars array          
            Car[] cars = new Car[max];
            int elements = Read(data, cars, out name);
            Array.Resize(ref cars, elements);
            //Array holding car(s) that consume least fuel
            Car[] smallestCons = BestEconomy(cars);
            //Array holding car(s) that can fit the most passengers
            Car[] biggest = Biggest(cars);

            //Student 1 data
            string nameS1;
            Car[] carsS1 = new Car[max];
            int elementsS1 = Read(dataS1, carsS1, out nameS1);
            Array.Resize(ref carsS1, elementsS1);
            Car[] biggestS1 = Biggest(carsS1);
            Car[] smallestConsS1 = BestEconomy(carsS1);

            //Student 2 data
            Car[] carsS2 = new Car[max];
            string nameS2;
            int elementsS2 = Read(dataS2, carsS2, out nameS2);
            Array.Resize(ref carsS2, elementsS2);
            Car[] biggestS2 = Biggest(carsS2);
            Car[] smallestConsS2 = BestEconomy(carsS2);

            //Comparing student car parameters
            Car[] biggestS = CompareSizes(biggestS1, biggestS2);
            Car[] combinedConsum = CombineArrays(smallestConsS1, smallestConsS2);

            PrintTable(cars, name, smallestCons, biggest, false);
            PrintTable(carsS1, nameS1, smallestConsS1, biggestS1, true);
            PrintTable(carsS2, nameS2, smallestConsS2, biggestS2, true);

            StudentComparisonTable(biggestS, biggestS1, biggestS2, nameS1, nameS2, combinedConsum, true);
        }

        public static int Read(string Data, Car[] cars, out string name) //method used to create car object array from a data file
        {
            using (StreamReader sr = new StreamReader(Data))
            {
                string line;
                int i = 0; //variable used to calculate the number of 'cars' objects
                int j = 0; //variable used to calculate the number of variables for one 'cars' object

                string[] parts = new string[3]; //array containing information(variables) for one 'cars' object

                name = sr.ReadLine();
                while ((line = sr.ReadLine()) != null)
                {
                    if (!string.IsNullOrWhiteSpace(line)) //checking if line is not empty
                    {
                        parts[j] = line; //filling array with info for one 'cars' object

                        if (j == 2) //checking if enough info was collected
                        {
                            cars[i] = new Car(parts[0], Convert.ToInt32(parts[1]), Convert.ToDouble(parts[2])); //creating a 'cars' object from collected info                           
                            j = -1; //because we don't need to add a whitespace to 'car' array
                            i++;
                        }
                        j++;
                    }
                }
                return i;
            }
        }

        static Car[] BestEconomy(Car[] cars) { //Method used to find student's car(s) that has least fuel consumption
            Car[] bestEconCars = new Car[max];
            double smallest = Double.MaxValue;
            int numOfCars = 0;

            for (int i = 0; i < cars.Length; i++) //simple linear search
            {
                if (cars[i]._consumption < smallest)
                {
                    smallest = cars[i]._consumption;
                }
            }

            for (int i = 0; i < cars.Length; i++)
            {
                if (cars[i]._consumption == smallest)
                {
                    bestEconCars[numOfCars] = cars[i];
                    numOfCars++;
                }
            }

            Array.Resize(ref bestEconCars, numOfCars);

            return bestEconCars;
        }

        static Car[] Biggest(Car[] cars) //Method used to find student's car(s) that can carry most passangers
        {
            Car[] biggestCars = new Car[max];
            int largest = int.MinValue;
            int numOfCars = 0;

            for (int i = 0; i < cars.Length; i++) //simple linear search
            {
                if (cars[i]._size > largest)
                {
                    largest = cars[i]._size;
                }
            }

            for (int i = 0; i < cars.Length; i++) //simple linear search
            {
                if (cars[i]._size == largest)
                {
                    biggestCars[numOfCars] = cars[i];
                    numOfCars++;
                }
            }

            Array.Resize(ref biggestCars, numOfCars);

            return biggestCars;
        }

        static Car[] CompareSizes(Car[] biggestS1, Car[] biggestS2) //Method used to determine which student has car that can carry most passengers
        {
            int numOfSpacesS1 = biggestS1[0]._size;
            int numOfSpacesS2 = biggestS2[0]._size;

            Car[] combined = new Car[max];

            if (numOfSpacesS1 > numOfSpacesS2)
            {
                return biggestS1;
            }
            else if (numOfSpacesS1 == numOfSpacesS2)
            {
                for (int i = 0; i < biggestS1.Length; i++)
                {
                    combined[i] = biggestS1[i];
                }

                for (int i = 0; i < biggestS2.Length; i++)
                {
                    combined[biggestS1.Length + i] = biggestS2[i];
                }

                Array.Resize(ref combined, (biggestS1.Length + biggestS2.Length));
            }
            else
            {
                return biggestS2;
            }

            return combined;
        }

        static Car[] CombineArrays(Car[] array1, Car[] array2) //Method used top combine two arrays into one
        {
            int size = array1.Length + array2.Length;
            Car[] combined = new Car[size];

            for (int i = 0; i < array1.Length; i++)
            {
                combined[i] = array1[i];
            }
            for (int i = 0; i < array2.Length; i++)
            {
                combined[array1.Length + i] = array2[i];
            }

            return combined;
        }

        static void PrintTable(Car[] cars, string name, Car[] smallestCons, Car[] biggest, bool noReset) //Printing out data to represent each student's data 
        {
            //Creating a writer
            string Result = "Result.txt";
            StreamWriter writer = new StreamWriter(Result, noReset);

            using (writer)
            {
                const string horizontal = "--------------------------------------";
                const string horizontal2 = "|------------------------------------|";

                const string top = "| Model        | Size  | Consumption |\n" +
                     "|--------------|-------|-------------|";

                //Generating the to of the table
                writer.WriteLine(horizontal + "\n| {0, -34} |\n"
                    + horizontal2 + "\n" + top, name);
                //Printing out info about each car
                for (int i = 0; i < cars.Length; i++)
                {
                    writer.WriteLine("| {0,-12} | {1,-5} | {2,-11} |"
                                    , cars[i]._model, cars[i]._size, cars[i]._consumption);
                }
                //Printing out cars with least fuel consumption
                writer.WriteLine(horizontal2 + "\n|Least consumption of fuel           |\n" + horizontal2);
                for (int i = 0; i < smallestCons.Length; i++)
                {
                    writer.WriteLine("| {0,-12} | {1,-5} | {2,-11} |",
                    smallestCons[i]._model, smallestCons[i]._size, smallestCons[i]._consumption);
                }
                //Printing out cars that can carry the most passengers
                writer.WriteLine(horizontal2 + "\n|Can carry most passengers           |\n" + horizontal2);
                for (int i = 0; i < biggest.Length; i++)
                {
                    writer.WriteLine("| {0,-12} | {1,-5} | {2,-11} |",
                    biggest[i]._model, biggest[i]._size, biggest[i]._consumption);
                }
                writer.WriteLine(horizontal + "\n\n");
            }
        }

        static void StudentComparisonTable(Car[] biggestS, Car[] biggestS1, Car[] biggestS2, 
            string nameS1, string nameS2, Car[] combinedConsum, bool noReset) //Printing the table which compares cars of 2 students
        {
            //Creating a writer
            string Result = "Result.txt";
            StreamWriter writer = new StreamWriter(Result, noReset);

            using (writer)
            {
                string name;
                const string horizontal = "-----------------------------------------------------";
                const string horizontal2 = "|---------------------------------------------------|";

                //Checking which student has car(s) that can carry the most passengers
                if (biggestS == biggestS1)
                {
                    name = nameS1;
                }
                else if (biggestS == biggestS2)
                {
                    name = nameS2;
                }
                else
                {
                    name = "Both students";
                }
                //Printing out which student's car(s) can carry the most passengers
                writer.WriteLine(horizontal
                        + "\n|{0, -50} |" + "\n|found the car(s) that can carry the most passengers|\n"
                        + horizontal2
                        + "\n| Model             | Size          | Consumption   |\n"
                        + horizontal2, name);
                for (int i = 0; i < biggestS.Length; i++)
                {
                    writer.WriteLine("| {0, -17} | {1, -13} | {2, -13} |",
                        biggestS[i]._model, biggestS[i]._size, biggestS[i]._consumption);
                }

                //Printing out cars from both students's lists that have least fuel consumption
                writer.WriteLine(horizontal2
                        + "\n|Cars from both students's lists that have least    |\n"
                        + "|fuel consumption                                   |\n"
                        + horizontal2);
                for (int i = 0; i < combinedConsum.Length; i++)
                {
                    writer.WriteLine("| {0, -17} | {1, -13} | {2, -13} |",
                        combinedConsum[i]._model, combinedConsum[i]._size, combinedConsum[i]._consumption);
                }
                writer.WriteLine(horizontal);
            }
            
        }
    }
}
